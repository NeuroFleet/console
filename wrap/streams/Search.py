from reactor.shortcuts import *

################################################################################

@Reactor.wamp.register_middleware('media.Web.Search.Google')
class Google(Reactor.wamp.Profiler):
    def on_init(self, details):
        self.parser = eventlet.import_patched('feedparser')

    ############################################################################

    @Reactor.wamp.register_topic(u'web.search.google.search')
    def search_history(self, msg):
        pass

    ############################################################################

    def search_query(self, query, offset, pager=10):
        lookup = {
            'fields': '*',
            'q':     query,
            'start': (offset * pager),
            'num':   pager,
        }

        self.publish(u'web.search.google.search', lookup)

        lookup.update({
            'key':    'AIzaSyA2W41chN8KBhqd6sVrxTkd9yBiXWIn_qI',
            'cx':     '012531542993623382654:9ci778pznwq',
        })

        req = requests.get('https://www.googleapis.com/customsearch/v1', query=lookup).json()

        return req.json()

    #***************************************************************************

    @Reactor.wamp.register_method(u'web.search.google.simple')
    def search_simple(self, query, count=25):
        data = self.search_query(query, 0)

        resp = {
            'engine': data['context']['title'],
            'query':  query,
            'result': [],
            'total':  data['searchInformation']['totalResults'],
            'cycle':  data['searchInformation']['searchTime'],
        }

        offset = 0

        while (len(resp['result']) < count) and (len(resp['result']) < resp['total']):
            data = self.search_query(query, offset)

            for item in data['queries']['items']:
                resp['result'].append(item)

            offset += 1

        return resp

