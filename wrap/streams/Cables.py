from reactor.shortcuts import *

################################################################################

@Reactor.wamp.register_middleware('media.Web.Cables.Feeds')
class Feeds(Reactor.wamp.Profiler):
    def on_init(self, details):
        self.parser = eventlet.import_patched('feedparser')

        self.feeds = [
            
        ]

    ############################################################################

    @Reactor.wamp.register_topic(u'perso.cables.sync')
    def cables_sync(self, stce):
        pass

    #***************************************************************************

    @Reactor.wamp.register_topic(u'perso.cables.update')
    def cables_update(self, stce):
        pass

    #***************************************************************************

    @Reactor.wamp.register_topic(u'perso.cables.story')
    def cables_story(self, msg):
        pass

    ############################################################################

    @Reactor.wamp.register_method(u'perso.cables.list')
    def feeds_list(self):
        return self.feeds

    #***************************************************************************

    @Reactor.wamp.register_method(u'perso.cables.sync')
    def feeds_sync(self):
        for link in self.feeds:
            self.queue.spawn_n(self.feeds_update, link)

    ############################################################################

    @Reactor.wamp.register_method(u'perso.cables.parse')
    @Reactor.wamp.register_method(u'perso.cables.append')
    def feeds_fetch(self, url):
        resp = feedparser.parse(url)

        return resp

    #***************************************************************************

    @Reactor.wamp.register_method(u'perso.cables.update')
    def feeds_update(self, url):
        feed = self.feeds_fetch(url)

        return d.feed.get('title', '')

        rss = RssFeed.objects.get(pk=rss_id)

        if rss.proxy:
            for entry in rss.proxy.entries:
                upsert(StoryPost, *StoryPost.from_feedparser(rss.owner.pk, entry))

            rss.title      = rss.proxy.feed.title
            rss.summary    = rss.proxy.feed.description

            if hasattr(rss.proxy.feed, 'updated'):
                rss.publish_at = dt_parser(rss.proxy.feed.updated)
            else:
                rss.publish_at = timezone.now()

            rss.save()

            print "*) Processed RSS Feed : %(link)s" % rss.__dict__
        else:
            print "*) Skipped RSS Feed : %(link)s" % rss.__dict__

    #***************************************************************************

    @Reactor.wamp.register_method(u'perso.cables.delete')
    def feeds_delete(self, url):
        if url in self.feeds:
            self.feeds.remove(url)

    ############################################################################

    def piler(self, listing, callback, *args, **kwargs):
        pile = eventlet.GreenPile(self.pool)

        for item in listing:
            pile.spawn(callback, item, *args, **kwargs)

        # since the pile is an iterator over the results,
        # you can use it in all sorts of great Pythonic ways
        return [x for x in pile]

################################################################################

from pattern.web import Facebook, NEWS, COMMENTS, LIKES

@Reactor.wamp.register_middleware('media.Web.Cables.News')
class News(Reactor.wamp.Profiler):
    def on_init(self, details):
        self.feeds = [
            
        ]

    ############################################################################

    @Reactor.wamp.register_topic(u'perso.fb_news.person')
    def facebook_profile(self, profile):
        pass

    #***************************************************************************

    @Reactor.wamp.register_topic(u'perso.fb_news.story')
    def facebook_story(self, profile, story):
        print repr(post.id)
        print repr(post.text)
        print repr(post.url)
        if post.comments > 0:
            print '%i comments' % post.comments 
            print [(r.text, r.author) for r in fb.search(post.id, type=COMMENTS)]
        if post.likes > 0:
            print '%i likes' % post.likes 
            print [r.author for r in fb.search(post.id, type=LIKES)]

    ############################################################################

    @Reactor.wamp.register_method(u'perso.news.facebook')
    def sync_fb(self, token, *profiles):
        fb = Facebook(license=token)

        for uid in profiles:
            me = fb.profile(id=uid) # user info dict

            self.publish(u"reactor.perso.fb_news.person", me)

            for post in fb.search(me['id'], type=NEWS, count=100):
                self.publish(u"reactor.perso.fb_news.person", me, post)

