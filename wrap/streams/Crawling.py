from reactor.shortcuts import *

# http://daringfireball.net/2009/11/liberal_regex_for_matching_urls
url_regex = re.compile(r'\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))')

from pattern.web import URL, DOM, plaintext, Crawler

################################################################################

class Walker(Crawler):
    def __init__(self, parent, *args, **kwargs):
        self.parent = parent

        super(Walker, self).__init__(*args, **kwargs)

    def visit(self, link, source=None):
        print Reactor.wamp.publish(u"reactor.perso.walker.visited", link.url, link.referer)

    def fail(self, link):
        print Reactor.wamp.publish(u"reactor.perso.walker.failed", link.url)

################################################################################

#from django.core.cache import cache

def crawl_fetch(spider, link):
    print "[crawler] Fetching '%s' using %s ..." % (link,spider)

    data = ''

    with eventlet.Timeout(5, False):
        data = urllib2.urlopen(link).read()

    cache.set('crawler+'+link, data, timeout=180)

    print Reactor.wamp.publish(u"reactor.perso.crawler.fetch", link, data)

    for handler in (crawl_process,crawl_follow,crawl_cleanup):
        Reactor.rq.enqueue(handler, 'crawler')(spider, link)

################################################################################

def crawl_process(spider, link):
    data = cache.get('crawler+'+link, "")

    print "[crawler] Processing '%s' using %s ..." % (link,spider)

    print Reactor.wamp.publish(u"reactor.perso.crawler.process", link, data)

    return

    tree = DOM(data)

    for entry in tree('div.entry')[:3]: # Top 3 reddit entries.
        for anchor in e('a.title')[:1]: # First <a class="title">.
            print repr(plaintext(anchor.content))

#*******************************************************************************

def crawl_follow(spider, link):
    data = cache.get('crawler+'+link, "")

    print "[crawler] Following '%s' using %s ..." % (link,spider)

    resp = []

    for match in url_regex.finditer(data):
        resp.append(match.group(0))

    tree = DOM(data)

    for anchor in tree('a'):
        if 'href' in anchor.attrs:
            resp.append(anchor.attrs['href'])

    for target in resp:
        #qs = self.listing('web_crawler', {
        #    'spider': spider,
        #    'target': target,
        #})

        qs = [
            key
            for key in ('crawler','spider')
            if (cache.get(key+'+'+link) is not None)
        ]

        if len(qs)==0:
            print Reactor.wamp.publish("reactor.perso.crawler.referer", link, target)

            Reactor.rq.enqueue(crawl_fetch, 'crawler')(spider, target)

#*******************************************************************************

def crawl_cleanup(spider, link):
    cache.delete('crawler+'+link)

    cache.set('spider+'+link, datetime.now(), timeout=None)

################################################################################

@Reactor.wamp.register_middleware('media.Web.Crawling.Spiders')
class Spiders(Reactor.wamp.Profiler):
    def on_init(self, details):
        self.history = set()

    ############################################################################

    @Reactor.wamp.register_topic(u'reactor.perso.crawler.process')
    def crawler_processed(self, spider, target):
        pass
        #self.upsert('web_crawler', {
        #    'spider':     spider,
        #    'target':     target,
        #},{
        #    'visited_on': datetime.now(),
        #})

    #***************************************************************************

    @Reactor.wamp.register_topic(u'reactor.perso.crawler.follow')
    def crawler_followed(self, spider, target):
        pass

    #***************************************************************************

    @Reactor.wamp.register_topic(u'reactor.perso.crawler.referer')
    def crawler_visited(self, address, referer):
        pass

    ############################################################################

    @Reactor.wamp.register_method(u'reactor.perso.crawler')
    def crawler(self, spider, *starters):
        for link in starters:
            print "Using spider '%s' for : %s" % (spider, link)

            Reactor.rq.enqueue(crawl_fetch, 'crawler')(spider, link)

    #***************************************************************************

    @Reactor.wamp.register_method(u'reactor.perso.walker')
    def walker(self, spider, *starters):
        p = Walker(self, links=starters, delay=3, throttle=3)

        while not p.done:
            p.crawl(method=DEPTH, cached=False, throttle=3)

