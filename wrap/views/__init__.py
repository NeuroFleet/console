from nucleon.console.shortcuts import *

################################################################################

@login_required
@render_to('console/home.html')
def homepage(request):
    return {
        'stats': [
    dict(color='blue',   icon='image',   label="Domains", value=0, total=250),
    dict(color='red',    icon='cubes',   label="Spiders", value=0, total=50),
    dict(color='green',  icon='disk',    label="URLs",    value=0, total=5000),
    dict(color='purple', icon='sitemap', label="Runs",    value=0, total=15000),
        ],
        'frm_crawl': CrawlerForm()
    }

#*******************************************************************************

@login_required
@render_to('console/streams.html')
def do_streams(request):
    return {
        'stats': [
    dict(color='blue',   icon='bullhorn', label="WAMP topics",      value=len(Reactor.wamp.topics),  total=700),
    dict(color='red',    icon='link',     label="WAMP methods",     value=len(Reactor.wamp.methods), total=400),
    dict(color='green',  icon='plug',     label="WAMP middlewares", value=len(Reactor.wamp.classes), total=40),
    dict(color='purple', icon='bookmark', label="WAMP sessions",    value=0, total=250),
        ],
    }

################################################################################

from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^streams$',                       do_streams, name='streams'),

    url(r'^$',                              homepage),
]

