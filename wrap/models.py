from nucleon.console.shortcuts import *

#*******************************************************************************

PROCESS_ENGINEs = (
    ('sh',     "Z Shell"),
    ('js',     "JavaScript"),
    ('py',     "Python"),
    ('sql',    "SQL runner"),
    ('sparql', "SparQL"),
    ('cypher', "Neo4j Cypher"),
)

SCRIPT_ENGINEs = (
    ('thread', "Multi-Threading"),
    ('docker', "Docker execution"),
    ('mesos',  "Mesos execution"),
)

################################################################################

@Reactor.orm.register_model('identity_ssh')
class IdentitySSH(models.Model):
    owner   = models.ForeignKey(Identity, related_name='ssh_keys')
    alias   = models.CharField(max_length=48, blank=True)

    public  = models.TextField()
    private = models.TextField(blank=True)

#*******************************************************************************

@Reactor.orm.register_model('identity_pgp')
class IdentityPGP(models.Model):
    owner   = models.ForeignKey(Identity, related_name='pgp_keys')
    alias   = models.CharField(max_length=48, blank=True)

    public  = models.TextField()
    private = models.TextField(blank=True)

#*******************************************************************************

@Reactor.orm.register_model('identity_exe')
class IdentityExecutionner(models.Model):
    owner   = models.ForeignKey(Identity, related_name='executers', blank=True, null=True, default=None)
    alias   = models.CharField(max_length=128)

    engine  = models.CharField(max_length=48, default='thread', choices=PROCESS_ENGINEs)
    target  = models.CharField(max_length=256, blank=True)
    narrow  = models.CharField(max_length=64, blank=True)

    pseudo  = models.CharField(max_length=64, blank=True)
    passwd  = models.CharField(max_length=64, blank=True)
    config  = models.TextField(default='{}')

    public  = property(lambda self: (self.owner is None))

#*******************************************************************************

@Reactor.orm.register_model('identity_run')
class IdentityRunner(models.Model):
    owner   = models.ForeignKey(Identity, related_name='runners', blank=True, null=True, default=None)
    alias   = models.CharField(max_length=128)

    engine  = models.CharField(max_length=48, default='sh', choices=SCRIPT_ENGINEs)
    target  = models.CharField(max_length=256, blank=True)

    narrow  = models.CharField(max_length=64, blank=True)
    config  = models.TextField(default='{}')

    public  = property(lambda self: (self.owner is None))

################################################################################

@Reactor.orm.register_model('process_model')
class ProcessModel(models.Model):
    owner   = models.ForeignKey(Identity, related_name='processes')
    alias   = models.CharField(max_length=48, blank=True)

    runner  = models.ForeignKey(IdentityRunner, related_name='processes')
    source  = models.TextField()

    def __invoke__(self, narrow, *args, **kwargs):
        resp = ProcessInstance(model=self, alias=narrow)
        resp = self.runner.execute(self.source)

        return resp

#*******************************************************************************

@Reactor.orm.register_model('process_instance')
class ProcessInstance(models.Model):
    model   = models.ForeignKey(ProcessModel, related_name='instances')
    when    = models.CharField(max_length=48)

    stdout  = models.TextField(blank=True)
    stderr  = models.TextField(blank=True)

################################################################################

@Reactor.orm.register_model('backend_arch')
class BackendArch(models.Model):
    owner   = models.ForeignKey(Identity, related_name='backends')
    alias   = models.CharField(max_length=48, blank=True)

    public  = models.TextField()
    private = models.TextField(blank=True)

    @property
    def records(self):
        pass

    def populate(self):
        resp = {}

        if self.engine=='thread':
            with Pool(processes=4) as queue:
                for d in self.dimensions.all():
                    self.exec_dimension_thread(d)

    def exec_dimension_thread(self, d):
        pass

#*******************************************************************************

@Reactor.orm.register_model('backend_dimension')
class BackendDimension(models.Model):
    backend = models.ForeignKey(BackendArch, related_name='dimensions')
    alias   = models.CharField(max_length=48, blank=True)

    runner  = models.ForeignKey(IdentityRunner, related_name='processes')
    source  = models.TextField()

    def __invoke__(self, *args, **kwargs):
        resp = self.runner.execute(self.source)

        return resp

#*******************************************************************************

@Reactor.orm.register_model('backend_process')
class BackendProcess(models.Model):
    backend = models.ForeignKey(BackendArch, related_name='processes')
    alias   = models.CharField(max_length=48, blank=True)

    process = models.ForeignKey(ProcessModel, related_name='processes')
    mapping = models.TextField(default='{}')

