from django.core.management.base import BaseCommand, CommandError

from subprocess import Popen

from nucleon.infosec.shortcuts import *

################################################################################

class ProcessWrapper(Mutable):
    def __init__(self, parent, provider, key, *params, **context):
        self._cmd = parent
        self._hnd = provider

        self._key = key
        self._arg = params
        self._cfg = context

        self._obj = self.trigger('processus', handler, params, context)

    command  = property(lambda self: self._cmd)
    provider = property(lambda self: self._hnd)

    alias    = property(lambda self: self._key)
    params   = property(lambda self: self._cfg)
    config   = property(lambda self: self._cfg)

    process  = property(lambda self: self._obj)

    owner    = property(lambda self: self.command.owner)
    queue    = property(lambda self: self.command.queue)

    def spawn(self, method, *args, **kwargs):
        self

#*******************************************************************************

import multiprocessing

class ThreadingWrapper(ProcessWrapper):
    def initialize(self, provider, args, kwargs):
        p = multiprocessing.Process(target=provider, args=tuple([x for x in args]), kwargs=kwargs)

        return p

    @property
    def identifier(self):
        if self.process:
            return self.process.pid
        else:
            return None

    @property
    def is_running(self):
        return self.process.running

    def serve(self):
        self.process.start()

    def attach(self):
        self.process.join()

################################################################################

class Command(BaseCommand):
    help = 'Run a custom user process.'

    def add_arguments(self, parser):
        parser.add_argument('provider',          type=str)
        parser.add_argument('alias',             type=str)

        parser.add_argument('params', nargs='+', type=str)

        #parser.add_argument('lists', nargs='+', type=str)

    def handle(self, *args, **options):
        self.owner = 
        self.queue = eventlet.GreenQueue(20)

        print "Using scanner '%s' :" % options['module']

        for dest in lst:
            queue.spawn_n(self.runner, target, options['module'], dest)

        queue.wait()

    @property
    def is_running(self):
        resp = True

        for wrp in self.wrappers:
            if wrp.is_running:
                resp = False

        return resp

    def runner(self, lst, mod, dest):
        ret = Reactor.rq.enqueue(scan_addr, lst.id, mod, dest)

        ret.wait()

        print "\t-> Scanned '%s' ..." % dest

