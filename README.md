# Shell commands #

* voodoo :
* zombie :

# Features #

* Flavor detection on repos dynamically under specific path naming convention.
* C.I & DevOps oriented annotation of vhosts (CDN / API / Mail / CRON / Instances / Bots / Backends / Shipping / ACL).

### List of packages ###

* apache2
* tomcat7
* jboss-as4
* grails
* rails
* gunicorn
* JAX-RS

To run inside Docker:
```
docker pull it2use/kungfu

docker run -p 8081:80 -v /srv/vhost/example.tld:/app --name vhost-example.tld it2use/kungfu
```
